using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace flappy_razor.Models {
    public class BirdModel {
        public int DistanceFromGround { get; protected set; } = 100;
        public int LeftSide {get; protected set;} = 220;
        public int Width {get; protected set;} = 60;
        public int Height {get; protected set;} = 45;
        public int RightSide {get; protected set;} = 500;
        private int ceiling;
        private int gameSpaceWidth;
        private int gameSpaceHeight;
        private int gravity = -2;
        private int yVelocity = 0;
        private int flapStrength = 15;

        public BirdModel(int gameWidth, int gameHeight){
            RightSide = LeftSide + Width;
            gameSpaceHeight = gameHeight;
            gameSpaceWidth = gameWidth;
            ceiling = gameSpaceHeight - Height;
        }
        public void Fall() {
            yVelocity += gravity;
        }
        public void Flap() {
            yVelocity = 0;
            yVelocity += flapStrength;
        }

        public bool Update(List<PipeModel> pipes) {
            Fall();
            
            // Did we hit the ground? 
            if (DistanceFromGround >= Math.Abs(yVelocity)){
                DistanceFromGround += yVelocity;
            } else {
                DistanceFromGround = 0;
            }

            // Clamp the bird to the screen.
            if (DistanceFromGround > ceiling) {
                DistanceFromGround = ceiling;
            }
            bool collision = false;
            foreach( var pipe in pipes){
                if (pipe.IsActive){
                    if (CheckCollision(pipe)){
                        collision = true;
                        break;
                    }
                }
            }
            return collision;
        }

        private bool CheckCollision(PipeModel pipe){
                             //    Top Side               Bottom of top pipe          Bottom side         Top of Bottom pipe
            // bool yIsSafe = ((DistanceFromGround + 45) <= pipe.DistanceFromBottom) && (DistanceFromGround >= pipe.DistanceFromTop);
            //         // Pipe left side is past our right side     Pipe right side is past our left side.
            // bool XIsSafe = (pipe.DistanceFromLeft >= 280) || ((pipe.DistanceFromLeft + 60) <= (220));

            // Check if we are even near the pipe on the X axis.
            // This is IF WE ARE OVERLAPPING THE PIPE on the X axis.
            if (pipe.DistanceFromLeft <= RightSide && pipe.DistanceFromLeft >= LeftSide - 60 ){
                // Then check if we are safe from the bottom and top parts
                // This is IF WE ARE OVERLAPPING THE PIPE on the Y axis.
                if ((DistanceFromGround <= pipe.DistanceFromBottom - pipe.Gap) || (DistanceFromGround + Height >= pipe.DistanceFromBottom)){
                    return true;
                }
            }
            // if we're not overlapping the pipe in either axis, we can return false;
            // This can be rewritten to return the boolean evaluation of the above if statements, but this is more expressive.
            return false;

            // As long as we are safe on the X axis, we're fine
            // As long as we are safe on the Y axis, we're still fine
            // If we're not safe on either, we lose.
            // return !(XIsSafe || yIsSafe);
        }
    }

}