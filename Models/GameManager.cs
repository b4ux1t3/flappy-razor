using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

namespace flappy_razor.Models {
    public class GameManager {
        public BirdModel Bird { get; private set; }
        public List<PipeModel> IncomingPipes { get; private set; }
        public bool IsRunning { get; private set; } = false;
        public event EventHandler MainLoopCompleted;
        public int GameWidth {get; protected set;} = 500;
        public int GameHeight {get; protected set;} = 730;
        public int SkyHeight {get; protected set;} = 580; 
        public static int Score {get; protected set;} = 0;
        private PipeModel flagged;
        private int spacing = 200;
        private int numPipes = 6;
        private int pipeStart = 500;
        private int frameTime = 16;

        public GameManager() {
            Bird = new BirdModel(GameWidth, GameHeight);
            IncomingPipes = new List<PipeModel>();
        }

        public async void MainLoop() {
            IsRunning = true;
            while (IsRunning) {
                // We only need to check the first pipe in the queue, since it's the one we're likely to colide with.
                bool DidBirdCollide = Bird.Update(IncomingPipes);

                UpdatePipes();

                if (Bird.DistanceFromGround <= 0 || DidBirdCollide) {
                    GameOver();
                }

                MainLoopCompleted?.Invoke(this, EventArgs.Empty);
                await Task.Delay(frameTime );
            }
        }

        public void UpdatePipes(){
            flagged = null;

                foreach(var Pipe in IncomingPipes){
                    Pipe.Move();
                    if (Pipe.DistanceFromLeft < - 60) {
                        flagged = Pipe;
                    }
                    if (Pipe.IsActive && Pipe.DistanceFromLeft <= (GameWidth / 2) - (Bird.Width / 2) - 60){
                        Score++;
                        Pipe.SetInactive();
                        Console.Out.WriteLine("Score: " + Score.ToString());
                    }
                }

                if (flagged != null) {
                    IncomingPipes.Remove(flagged);
                    IncomingPipes.Add(new PipeModel(pipeStart + (numPipes - (pipeStart / spacing)) * spacing));
                }
        }

        public void StartGame() {
            if (!IsRunning) {
                Bird = new BirdModel(GameWidth, SkyHeight);
                IncomingPipes = new List<PipeModel>();
                for (int i = 0; i < numPipes; i++){
                    IncomingPipes.Add(new PipeModel(pipeStart + (i * spacing)));
                }
                MainLoop();
            }
        }

        public void GameOver() {
            IsRunning = false;
        }

        public void TryFlap() {
            if (IsRunning){
                Bird.Flap();
            }
        }
    }
}