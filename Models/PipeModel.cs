using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace flappy_razor.Models {
    public class PipeModel {
        public int DistanceFromLeft { get; private set; }
        public int DistanceFromBottom { get; private set; } = new Random().Next(580/2, 580);
        public int DistanceFromTop {get; private set;}
        public int Speed {get; protected set;} = 2;
        public int Gap {get; protected set;}= 150;
        public bool IsActive {get; protected set;} = true;
        public PipeModel (int _DistanceFromLeft){
            // We always want a specific pixel difference.
            DistanceFromTop = 580 - DistanceFromBottom + Gap;
            DistanceFromLeft = _DistanceFromLeft;
        }
        public void Move(){
            DistanceFromLeft -= Speed;
        }        

        public void SetInactive(){
            IsActive = false;
        }
    }

}